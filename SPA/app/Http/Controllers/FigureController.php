<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\figure;
class FigureController extends Controller
{
   // all figures
   public function index()
   {
       $figures = figure::all()->toArray();
       return array_reverse($figures);
   }

   // add book
   public function add(Request $request)
   {
       $figure = new figure([
           'name' => $request->input('name'),
           'marca' => $request->input('marca')
       ]);
       $figure->save();

       return response()->json('The figure successfully added');
   }

   // edit figure
   public function edit($id)
   {
       $figure = figure::find($id);
       return response()->json($figure);
   }

   // update figure
   public function update($id, Request $request)
   {
       $figure = figure::find($id);
       $figure->update($request->all());

       return response()->json('The figure successfully updated');
   }

   // delete figure
   public function delete($id)
   {
       $figure = figure::find($id);
       $figure->delete();

       return response()->json('The figure successfully deleted');
   }
}
